# Open lwIP with NAT for the esp8266

This version of lwIP can be dropped into the esp-open-sdk. It adds neocat's NAPT extensions: https://github.com/NeoCat/esp8266-Arduino/commit/4108c8dbced7769c75bcbb9ed880f1d3f178bcbe

Fixes some issues I had with checksums and timers an can be used for full WiFi repeater functionality.
